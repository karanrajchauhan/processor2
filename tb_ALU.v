`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer:
//
// Create Date:   20:48:25 04/11/2018
// Design Name:   ALU
// Module Name:   X:/Documents/Processor2/tb_ALU.v
// Project Name:  Processor2
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture created by ISE for module: ALU
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////

module tb_ALU;

	// Inputs
	reg clk;
	reg rst;
	reg [1:0] alu_op;
	reg [15:0] operand_A;
	reg [15:0] operand_B;

	// Outputs
	wire [15:0] result;

	// Instantiate the Unit Under Test (UUT)
	ALU uut (
		.clk(clk), 
		.rst(rst), 
		.alu_op(alu_op), 
		.operand_A(operand_A), 
		.operand_B(operand_B), 
		.result(result)
	);

	always #1 clk = !clk;

	initial begin
		// Initialize Inputs
		clk = 0;
		rst = 1;
		alu_op = 0;
		operand_A = 0;
		operand_B = 0;

		// Wait 100 ns for global reset to finish
		#100 rst = 0;
        
		alu_op = 1;
		#4 alu_op = 2;
		#4 alu_op = 3;
		#4;
	
	end
      
endmodule

