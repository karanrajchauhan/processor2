`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    22:32:27 03/11/2018 
// Design Name: 
// Module Name:    seg_generator 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module seg_generator(
	input wire [15:0] data,
	output [6:0] left,
	output [6:0] midleft,
	output [6:0] midright,
	output [6:0] right
   );

	// set leds corresponding to value in 15:12 bits
	assign left = data[15:12]==0 ? 7'b1000000
					: data[15:12]==1 ? 7'b1111001
					: data[15:12]==2 ? 7'b0100100
					: data[15:12]==3 ? 7'b0110000
					: data[15:12]==4 ? 7'b0011001
					: data[15:12]==5 ? 7'b0010010
					: data[15:12]==6 ? 7'b0000010
					: data[15:12]==7 ? 7'b1111000
					: data[15:12]==8 ? 7'b0000000
					: data[15:12]==9 ? 7'b0010000
					: data[15:12]==10 ? 7'b0100000
					: data[15:12]==11 ? 7'b0000011
					: data[15:12]==12 ? 7'b0100111
					: data[15:12]==13 ? 7'b0100001
					: data[15:12]==14 ? 7'b0000100
					: data[15:12]==15 ? 7'b0001110
					: 7'b1111111;
	
	// set leds corresponding to value in 11:8 bits
	assign midleft = data[11:8]==0 ? 7'b1000000
					: data[11:8]==1 ? 7'b1111001
					: data[11:8]==2 ? 7'b0100100
					: data[11:8]==3 ? 7'b0110000
					: data[11:8]==4 ? 7'b0011001
					: data[11:8]==5 ? 7'b0010010
					: data[11:8]==6 ? 7'b0000010
					: data[11:8]==7 ? 7'b1111000
					: data[11:8]==8 ? 7'b0000000
					: data[11:8]==9 ? 7'b0010000
					: data[11:8]==10 ? 7'b0100000
					: data[11:8]==11 ? 7'b0000011
					: data[11:8]==12 ? 7'b0100111
					: data[11:8]==13 ? 7'b0100001
					: data[11:8]==14 ? 7'b0000100
					: data[11:8]==15 ? 7'b0001110
					: 7'b1111111;
					
	// set leds corresponding to value in 7:4 bits
	assign midright = data[7:4]==0 ? 7'b1000000
					: data[7:4]==1 ? 7'b1111001
					: data[7:4]==2 ? 7'b0100100
					: data[7:4]==3 ? 7'b0110000
					: data[7:4]==4 ? 7'b0011001
					: data[7:4]==5 ? 7'b0010010
					: data[7:4]==6 ? 7'b0000010
					: data[7:4]==7 ? 7'b1111000
					: data[7:4]==8 ? 7'b0000000
					: data[7:4]==9 ? 7'b0010000
					: data[7:4]==10 ? 7'b0100000
					: data[7:4]==11 ? 7'b0000011
					: data[7:4]==12 ? 7'b0100111
					: data[7:4]==13 ? 7'b0100001
					: data[7:4]==14 ? 7'b0000100
					: data[7:4]==15 ? 7'b0001110
					: 7'b1111111;
	
	// set leds corresponding to value in 3:0 bits
	assign right = data[3:0]==0 ? 7'b1000000
					: data[3:0]==1 ? 7'b1111001
					: data[3:0]==2 ? 7'b0100100
					: data[3:0]==3 ? 7'b0110000
					: data[3:0]==4 ? 7'b0011001
					: data[3:0]==5 ? 7'b0010010
					: data[3:0]==6 ? 7'b0000010
					: data[3:0]==7 ? 7'b1111000
					: data[3:0]==8 ? 7'b0000000
					: data[3:0]==9 ? 7'b0010000
					: data[3:0]==10 ? 7'b0100000
					: data[3:0]==11 ? 7'b0000011
					: data[3:0]==12 ? 7'b0100111
					: data[3:0]==13 ? 7'b0100001
					: data[3:0]==14 ? 7'b0000100
					: data[3:0]==15 ? 7'b0001110
					: 7'b1111111;
					
endmodule
