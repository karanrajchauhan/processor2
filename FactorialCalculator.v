`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    17:40:21 04/01/2018 
// Design Name: 
// Module Name:    FactorialCalculator 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////

/* NOTE: input cannot be greater than 12, otherwise it will require more than 32 bits */
module FactorialCalculator #(
	parameter integer INPUT_WIDTH = 4,
	parameter integer OUTPUT_WIDTH = 32)(
	input clk,
	input rst,
	input [INPUT_WIDTH-1:0] data,			// number to calculate factorial of
	output reg overflow,							// asserted if factorial not calculated successfully
	output [OUTPUT_WIDTH-1:0] result		// factorial of data
	);

	reg [1:0] stage = 2'b11;
	reg [INPUT_WIDTH-1:0] test;
	
	wire [7:0] conn06;
	wire [7:0] conn16;

	// stage1 mult0
	MULT_MACRO #(
		.DEVICE("SPARTAN6"),
		.LATENCY(1),
		.WIDTH_A(4),
		.WIDTH_B(4)
		) stage1mult0 (
		.P(conn06),
		.A(1),
		.B(data>1 ? 2 : 1),
		.CE(1), // 1-bit active high input clock enable
		.CLK(clk),
		.RST(rst)
	);
	
		// stage1 mult1
	MULT_MACRO #(
		.DEVICE("SPARTAN6"),
		.LATENCY(1),
		.WIDTH_A(4),
		.WIDTH_B(4)
		) stage1mult0 (
		.P(conn06),
		.A(1),
		.B(data>1 ? 2 : 1),
		.CE(1), // 1-bit active high input clock enable
		.CLK(clk),
		.RST(rst)
	);
	
		// stage1 mult0
	MULT_MACRO #(
		.DEVICE("SPARTAN6"),
		.LATENCY(1),
		.WIDTH_A(4),
		.WIDTH_B(4)
		) stage1mult0 (
		.P(conn06),
		.A(1),
		.B(data>1 ? 2 : 1),
		.CE(1), // 1-bit active high input clock enable
		.CLK(clk),
		.RST(rst)
	);
	
		// stage1 mult0
	MULT_MACRO #(
		.DEVICE("SPARTAN6"),
		.LATENCY(1),
		.WIDTH_A(4),
		.WIDTH_B(4)
		) stage1mult0 (
		.P(conn06),
		.A(1),
		.B(data>1 ? 2 : 1),
		.CE(1), // 1-bit active high input clock enable
		.CLK(clk),
		.RST(rst)
	);
	
		// stage1 mult0
	MULT_MACRO #(
		.DEVICE("SPARTAN6"),
		.LATENCY(1),
		.WIDTH_A(4),
		.WIDTH_B(4)
		) stage1mult0 (
		.P(conn06),
		.A(1),
		.B(data>1 ? 2 : 1),
		.CE(1), // 1-bit active high input clock enable
		.CLK(clk),
		.RST(rst)
	);
	
		// stage1 mult0
	MULT_MACRO #(
		.DEVICE("SPARTAN6"),
		.LATENCY(1),
		.WIDTH_A(4),
		.WIDTH_B(4)
		) stage1mult0 (
		.P(conn06),
		.A(1),
		.B(data>1 ? 2 : 1),
		.CE(1), // 1-bit active high input clock enable
		.CLK(clk),
		.RST(rst)
	);



	// OR it with 4{is_sign_diff}

	always @ (posedge clk)
	begin
	
		if (!rst)
		begin
		
			test <= (data - 4'b1111);
		
			// update stage logic
			stage <= stage + 2'b01;
			
			// overflow logic
			if (data > 4'b1100)
			begin
				overflow <= 1;
			end
		
		end
		
	end

endmodule
