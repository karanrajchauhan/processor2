`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer:
//
// Create Date:   20:44:40 04/11/2018
// Design Name:   CmdProcessor
// Module Name:   X:/Documents/Processor2/tb_CmdProcessor.v
// Project Name:  Processor2
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture created by ISE for module: CmdProcessor
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////

module tb_CmdProcessor;

	// Inputs
	reg clk;
	reg rst;
	reg keyboardclk =0;
	reg data =0;
	
	// Outputs
	wire tx;
	wire [7:0] led;

	reg [7:0] i = 8'h43;
	reg [7:0] a = 8'h1c;
	reg [7:0] b = 8'h32;
	reg [7:0] r = 8'h2d;
	reg [7:0] enter = 8'h5a;
	reg [7:0] space = 8'h29;

	// Instantiate the Unit Under Test (UUT)
	CmdProcessor uut (
		.clk(clk), 
		.rst(rst),
		.keyboardclk(keyboardclk),
		.data(data),
		.tx(tx),
		.led(led)
	);


	always #1 clk = !clk;

	initial begin
		// Initialize Inputs
		clk = 1;
		rst = 1;
		assign uut.did_receive_kbd = 1;
		
		// Wait 100 ns for global reset to finish
		#100 rst = 0;
		
		
////////////////////////////////// MODE I TEST /////////////////////////////////
	
//		 // store h1234 in mem[0] and h4321 in mem[1]
//		 uut.did_receive_kbd = 1;
//		 assign uut.led = i;
//		 #2 assign uut.led = enter;
//
//		 #2 assign uut.led = 8'b1;
//		 #2 assign uut.led = 8'b10;
//		 #2 assign uut.led = 8'b11;
//		 #2 assign uut.led = 8'b100;
//		 #2 assign uut.led = enter;
//		 
//		 #2 assign uut.led = 8'b100;
//		 #2 assign uut.led = 8'b11;
//		 #2 assign uut.led = 8'b10;
//		 #2 assign uut.led = 8'b1;
//		 #2 assign uut.led = enter;
//
//		 #2;


////////////////////////////////// MODE A TEST /////////////////////////////////

		assign uut.led = a;
		#2 assign uut.led = enter;
		#2 assign uut.led = 8'b1;				// 1
		#2 assign uut.led = 8'b1;				// 1
		#2 assign uut.led = 8'b00101001;		// SPACE
		#2 assign uut.led = 8'h79;				// +
//		#2 assign uut.led = 8'b00000001;		// -
//		#2 assign uut.led = 8'b00000010;		// ^
//		#2 assign uut.led = 8'b00000011;		// #
		#2 assign uut.led = 8'b00101001;		// SPACE
		#2 assign uut.led = 8'b10;				// 1
		#2 assign uut.led = enter;
		#4;

////////////////////////////////// MODE B TEST /////////////////////////////////

		assign uut.led = b;
		#2 assign uut.led = enter;
		
		#2 assign uut.led = 8'b1;				// 1
		#2 assign uut.led = 8'b00101001;		// SPACE
		#2 assign uut.led = 8'b1;				// 1
		#2 assign uut.led = 8'b00101001;		// SPACE
		#2 assign uut.led = 8'b1;				// 1
		#2 assign uut.led = enter;				// ENTER
		#2 assign uut.led = 8'b1;				// 1
		#2 assign uut.led = 8'b00101001;		// SPACE
		#2 assign uut.led = 8'b1;				// 1
		#2 assign uut.led = 8'b00101001;		// SPACE
		#2 assign uut.led = 8'b1;				// 1
		#2 assign uut.led = enter;				// ENTER
		#2 assign uut.led = 8'b1;				// 1
		#2 assign uut.led = 8'b00101001;		// SPACE
		#2 assign uut.led = 8'b1;				// 1
		#2 assign uut.led = 8'b00101001;		// SPACE
		#2 assign uut.led = 8'b1;				// 1
		#2 assign uut.led = enter;				// ENTER
		
		#2 assign uut.led = 8'b11;				// 11
		#2 assign uut.led = 8'b00101001;		// SPACE
		#2 assign uut.led = 8'b11;				// 11
		#2 assign uut.led = 8'b00101001;		// SPACE
		#2 assign uut.led = 8'b11;				// 11
		#2 assign uut.led = enter;				// ENTER
		#2 assign uut.led = 8'b11;				// 11
		#2 assign uut.led = 8'b00101001;		// SPACE
		#2 assign uut.led = 8'b11;				// 11
		#2 assign uut.led = 8'b00101001;		// SPACE
		#2 assign uut.led = 8'b11;				// 11
		#2 assign uut.led = enter;				// ENTER
		#2 assign uut.led = 8'b11;				// 11
		#2 assign uut.led = 8'b00101001;		// SPACE
		#2 assign uut.led = 8'b11;				// 11
		#2 assign uut.led = 8'b00101001;		// SPACE
		#2 assign uut.led = 8'b11;				// 11
		#2 assign uut.led = enter;				// ENTER
		
		#2 assign uut.led = r;
		#2 assign uut.led = enter;
		#4;
		
		$finish;

	end
      
endmodule
