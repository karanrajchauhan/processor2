`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    18:14:40 03/03/2018 
// Design Name: 
// Module Name:    ControlUnit 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module ControlUnit(
	input				clk,
	input 				rst,
	input 				glb_is_equal,
	input 		[3:0] 	instr_opcode,
	output reg 			halt,
	output reg 			jump,
	output reg 			reg_write,
	output reg 			mem_write,
	output reg 	[1:0]	reg_write_data_sel
	);

	always @ (*)
	// always @ (posedge clk)
	begin
	
		if (rst == 1)
		begin
			halt <= 1;
			jump <= 0;
			mem_write <= 0;
			reg_write <= 0;
			reg_write_data_sel <= 2'b0;
		end
		
		else
		begin
		
			case (instr_opcode)
			
				// halt
				4'b0000:
				begin
					halt <= 1;
					jump <= 0;
					mem_write <= 0;
					reg_write <= 0;
					reg_write_data_sel <= 2'b0;
				end
				
				// inc
				4'b0001:
				begin
					halt <= 0;
					jump <= 0;
					mem_write <= 0;
					reg_write <= 1;
					reg_write_data_sel <= 2'b10;	// todo: read frmo alu result
				end

				// jmp
				4'b0010:
				begin
					halt <= 0;
					jump <= 1;
					mem_write <= 0;
					reg_write <= 0;
					reg_write_data_sel <= 2'b0;
				end
				
				// jne
				4'b0011:
				begin
					halt <= 0;
					jump <= ~glb_is_equal;
					mem_write <= 0;				
					reg_write <= 0;
					reg_write_data_sel <= 2'b0;
				end
				
				// je
				4'b0100:
				begin
					halt <= 0;
					jump <= glb_is_equal;
					mem_write <= 0;				
					reg_write <= 0;
					reg_write_data_sel <= 2'b0;
				end
				
				// add
				4'b0101:
				begin
					halt <= 0;
					jump <= 0;
					mem_write <= 0;
					reg_write <= 1;
					reg_write_data_sel <= 2'b0;
				end
				
				// sub
				4'b0110:
				begin
					halt <= 0;
					jump <= 0;
					mem_write <= 0;
					reg_write <= 1;
					reg_write_data_sel <= 2'b0;
				end

				// xor
				4'b0111:
				begin
					halt <= 0;
					jump <= 0;
					mem_write <= 0;
					reg_write <= 1;
					reg_write_data_sel <= 2'b0;
				end

				// cmp
				4'b1000:
				begin
					halt <= 0;
					jump <= 0;
					mem_write <= 0;
					reg_write <= 0;
					reg_write_data_sel <= 2'b0;
				end

				// special instruction
				4'b1101:
				begin
					halt <= 0;
					jump <= 0;
					mem_write <= 0;
					reg_write <= 1;
					reg_write_data_sel <= 2'b0;
				end			
				
				// mov rn, num
				4'b1001:
				begin
					halt <= 0;
					jump <= 0;
					mem_write <= 0;
					reg_write <= 1;
					reg_write_data_sel <= 2'b0;
				end
				
				// mov rn, rm
				4'b1010:
				begin
					halt <= 0;
					jump <= 0;
					mem_write <= 0;
					reg_write <= 1;
					reg_write_data_sel <= 2'b0;
				end
				
				// SW mov [rn], rm
				4'b1011:
				begin
					halt <= 0;
					jump <= 0;
					mem_write <= 1;
					reg_write <= 0;
					reg_write_data_sel <= 2'b0;
				end
				
				// LW mov rn, [rm]
				4'b1100:
				begin
					halt <= 0;
					jump <= 0;
					mem_write <= 0;
					reg_write <= 1;
					reg_write_data_sel <= 2'b1;
				end
				
				// halt
				default:
				begin
					halt <= 1;
					jump <= 0;
					mem_write <= 0;
					reg_write <= 0;
					reg_write_data_sel <= 2'b0;
				end
			
			endcase
		
		end

	end
	
endmodule
