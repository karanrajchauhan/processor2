VC 			:= iverilog
VCFLAGS		:= -Wall
PROGRAM		:= processor
# VER_OBJS	:= $(wildcard *.v)
# VER_OBJS	:= CmdProcessor.v Memory.v ALU.v tb_CmdProcessor.v
# VER_OBJS	:= ALU.v tb_ALU.v
VER_OBJS	:= CPU.v RegisterFile.v ControlUnit.v ALU.v Memory.v tb_CPU.v
# VER_OBJS	:= Memory.v tb_Memory.v
TEST 		:= test
TEST_OBJS	:= test.v

all: $(PROGRAM)

t: $(TEST)

$(PROGRAM):
	$(VC) $(VCFLAGS) -o $@ $(VER_OBJS)

$(TEST):
	$(VC) $(VCFLAGS) -o $@ $(TEST_OBJS)
	./$(TEST)

run:
	./$(PROGRAM)

clean:
	rm $(PROGRAM) $(TEST) $(wildcard *.vcd)

remake: clean all run

ar: all run
