`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    19:10:51 04/07/2018 
// Design Name: 
// Module Name:    CmdProcessor 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////

//////////////////////////////// IMPORTANT NOTES /////////////////////////////////
//
// For all modes, user is required to enter exactly 4 hex bits
// User will have to reenter the state to go to after every "mega-instruction"
// For mode i if empty enter is pressed, that will be saved as 0's (halt instruction)
// For mode b if r is called without any inputs/incorrect inputs then it assumes 0's
//
//////////////////////////////////////////////////////////////////////////////////

	// get input from keyboard - write keyboard decoder to decode inputs
	// pass it to terminal screen
	// save it in current module. update state
	// finish taking inputs
	// add check for writing instruction out of memory

// TODO: matmul will have to be reset after every calculation
// TODO: confirm the numbers for keyboard inputs
// TODO: parameterize everything if synthesizing
// TODO: display rsults
// FIXME: do something about multiple enters -- was prev key enter flag

module CmdProcessor(
	input clk,
	input rst,
	input keyboardclk,
	input data,
	output wire tx,
//	output [7:0] led,
//	output reg [4:0] num_data_rec = 0,
//	output reg [1:0] mode,
	output test,
	output [5:0]	sp_num_to_print,

//	output [7:0] correct_led,
	
	//////////////////////////////////////// seq pritner
//	output reg sp_en = 0,
//	output [2:0]	sp_num_printed,

	//////////////////////////////////////// cpu
//	output reg [3:0] pc = 0-1,

	//////////////////////////////////////// matmul
//	input [3:0] msel,
//	output					matmul_done,
//	output reg				matmul_en = 0,
//	output reg 	[4:0]		in_mat_idx = 0,
//	output reg				matmul_rst = 1,

	//////////////////////////////////////// alu
//	output reg [3:0] alu_op,
//	output reg 	[15:0]	operandA,
//	output [15:0] alu_result,

	//////////////////////////////////////// 7 seg
	input wire clr,
	output wire [6:0] seg,	//7-segment display LEDs
	output wire [3:0] an,	//7-segment display anode enable
	output wire dp	

	);


/////////////////////////////////// PARAMETERS ///////////////////////////////////

	parameter log_imem_size = 4;
	parameter data_width = 16;
	parameter imem_init_addr = 0;

	parameter num_i_data = 2**log_imem_size + 1;
	parameter num_a_data = 3;
	parameter num_b_data = 18;


///////////////////////// CMDPROCESSOR CONTROL VARIABLES /////////////////////////

	wire				done;								// asserted when all background processes are done
	reg				ready_to_run = 0;				// ready to run, set after r is received
	wire 				did_receive_kbd;				// did we receive somethign frmo the keyboard
	reg	[1:0]		mode;								// 0=I, 1=A, 2=B
	reg	[4:0]		num_data_rec = 0;				// number of data inputs received. will go up to 18 (for matmul)
	reg 	[1:0]		buf_idx = 0;
	reg	[3:0]		data_buf[0:3];					// buffer to store data inputs from keyboard


////////////////////////////////// CPU VARIABLES /////////////////////////////////

	wire 				cpu_done = 1;
	reg 	[3:0]		pc = imem_init_addr-1;					// program counter for memory. initialize at startloc - 1 (1 will be added when i then enter is presed)

	// TODO: MAKE THE PROCESSOR WORKSP!!!

//////////////////////////////// iMEMORY VARIABLES ////////////////////////////////
//todo: make imem from mem

	reg 				imem_write_en = 0;
	reg 				imem_output_en = 0;
	reg	[3:0]		imem_addr = imem_init_addr-1;
	reg 	[15:0] 	imem_write_data;
	wire 	[15:0]	imem_read_data;
	
	Memory imemory (
		.clk       (clk),
		.rst       (rst),
		.output_en (imem_output_en),
		.write_en  (imem_write_en),
		.address   (imem_addr),
		.write_data(imem_write_data),
		.read_data (imem_read_data)
	);


////////////////////////////////// ALU VARIABLES //////////////////////////////////

	wire 				is_equal;
	reg 	[3:0]		alu_op;
	reg 	[15:0]	operandA;
	reg 	[15:0]	operandB;
	wire 	[15:0]	alu_result;

	ALU alu (
		.clk      (clk),
		.rst      (rst),
		.alu_op   (alu_op),
		.operand_A(operandA),
		.operand_B(operandB),
		.is_equal (is_equal),
		.result   (alu_result)
	);


///////////////////////////////// MATMUL VARIABLES /////////////////////////////////

	wire				matmul_done;
	reg				matmul_rst = 1;
	reg				matmul_en = 0;		// start doing mat mul when en is asserted
	reg 	[4:0]		in_mat_idx = 0;	// indexes into the input matrices storing flattened matrix
	reg	[15:0]	in_mat[0:17];		// input matrices flattened
	wire	[15:0]	out_mat[0:8];		// output matrix flattened

	MatMul matmul (
		.clk(clk),
		.rst( rst || matmul_rst ),
		.en (matmul_en),
		.a0 (in_mat[0]),
		.a1 (in_mat[1]),
		.a2 (in_mat[2]),
		.a3 (in_mat[3]),
		.a4 (in_mat[4]),
		.a5 (in_mat[5]),
		.a6 (in_mat[6]),
		.a7 (in_mat[7]),
		.a8 (in_mat[8]),
		.b0 (in_mat[9]),
		.b1 (in_mat[10]),
		.b2 (in_mat[11]),
		.b3 (in_mat[12]),
		.b4 (in_mat[13]),
		.b5 (in_mat[14]),
		.b6 (in_mat[15]),
		.b7 (in_mat[16]),
		.b8 (in_mat[17]),
		.c0 (out_mat[0]),
		.c1 (out_mat[1]),
		.c2 (out_mat[2]),
		.c3 (out_mat[3]),
		.c4 (out_mat[4]),
		.c5 (out_mat[5]),
		.c6 (out_mat[6]),
		.c7 (out_mat[7]),
		.c8 (out_mat[8]),
		.done(matmul_done)
	);


///////////////////////////////// KEYBOARD + UART /////////////////////////////////

	wire [7:0] led;
	wire [7:0] correct_led;
	wire is_transmitting;
	wire kbd_tx;
	
	keyboardtest kbd_inst(.clk (keyboardclk), .sys_clk(clk), .data(data), .led(led), .flag_o(did_receive_kbd)) ;

	key2ascii key2ascii_converter (
		.scan_code(led),
		.ascii_code(correct_led)
	);

	uart instance3(
		.uart_busy (is_transmitting),
		.uart_tx (kbd_tx),
		.uart_wr_i(did_receive_kbd),
		.uart_dat_i (correct_led),
		.sys_clk_i (clk), 
		.sys_rst_i (rst)
	);

	assign tx = sp_en == 1 ? sp_tx : kbd_tx;


/////////////////////////////////// HELPER FUNCTION ///////////////////////////////////
	function val2ascii;
		input [3:0] val;
		begin
			val2ascii = (val < 4'hA) ? (val + 8'h30) : (val + 8'h57);
		end
	endfunction
	
///////////////////////////////// SEQUENTIAL PRINTER /////////////////////////////////

	reg 			sp_en = 0;
	wire			sp_tx;
	
//	wire [5:0]	sp_num_to_print;
	wire [355:0] sp_data;

	assign sp_num_to_print = mode==2'b00 ? 6'b0101	// 1 for pc, 4 for start and end /r/n. NOTE: this is only valid for 16b imem size
								  : mode==2'b01 ? 6'b1000	// 4 for alu result, 4 for start and end /r/n
								  : 6'b110010;					// 36 for mat, 6 for space, 8 for /r/n

	// first two things to print will always be /r/n
	assign sp_data[7:0] = 8'h0A;
	assign sp_data[15:8] = 8'h0D;

	 // NOTE: assumed mode 10 is implicitly assumed in the following 5 assign cases
	 
	// pc, alu result, outmat[0]
	assign sp_data[23:16] = mode==2'b00 ? ( (imem_addr < 4'hA) ? (imem_addr + 8'h30) : (imem_addr + 8'h57) )
								 : mode==2'b01 ? ( (alu_result[15:12] < 4'hA) ? (alu_result[15:12] + 8'h30) : (alu_result[15:12] + 8'h57) )
								 : ( (out_mat[0][15:12] < 4'hA) ? (out_mat[0][15:12] + 8'h30) : (out_mat[0][15:12] + 8'h57) );
	// /r/n, alu result, outmat[0]							 
	assign sp_data[31:24] = mode==2'b00 ? 8'h0A
								 : mode==2'b01 ? ( (alu_result[11:8] < 4'hA) ? (alu_result[11:8] + 8'h30) : (alu_result[11:8] + 8'h57) )
								 : ( (out_mat[0][11:8] < 4'hA) ? (out_mat[0][11:8] + 8'h30) : (out_mat[0][11:8] + 8'h57) );
								 
	// /r/n, alu result, outmat[0]							 
	assign sp_data[39:32] = mode==2'b00 ? 8'h0D
								 : mode==2'b01 ? ( (alu_result[7:4] < 4'hA) ? (alu_result[7:4] + 8'h30) : (alu_result[7:4] + 8'h57) )
								 : ( (out_mat[0][7:4] < 4'hA) ? (out_mat[0][7:4] + 8'h30) : (out_mat[0][7:4] + 8'h57) );

	// alu result, outmat[0]
	assign sp_data[47:40] = mode==2'b01 ? ( (alu_result[3:0] < 4'hA) ? (alu_result[3:0] + 8'h30) : (alu_result[3:0] + 8'h57) )
								 : ( (out_mat[0][3:0] < 4'hA) ? (out_mat[0][3:0] + 8'h30) : (out_mat[0][3:0] + 8'h57) );
								 
	// /r/n, space
	assign sp_data[55:48] = mode==2'b01 ? 8'h0A
								 : 8'h20;
								 
	// /r/n, outmat[1]
	assign sp_data[63:56] = mode==2'b01 ? 8'h0D
								 : ( (out_mat[1][15:12] < 4'hA) ? (out_mat[1][15:12] + 8'h30) : (out_mat[1][15:12] + 8'h57) );
								 
	// todo: replace all indices from here onwards	
	// outmat[1]
	assign sp_data[71:64] = ( (out_mat[1][11:8] < 4'hA) ? (out_mat[1][11:8] + 8'h30) : (out_mat[1][11:8] + 8'h57) );
	assign sp_data[79:72] = ( (out_mat[1][7:4] < 4'hA) ? (out_mat[1][7:4] + 8'h30) : (out_mat[1][7:4] + 8'h57) );
	assign sp_data[87:80] = ( (out_mat[1][3:0] < 4'hA) ? (out_mat[1][3:0] + 8'h30) : (out_mat[1][3:0] + 8'h57) );
	
	// outmat[2]
	assign sp_data[95:88] = ( (out_mat[2][15:12] < 4'hA) ? (out_mat[2][15:12] + 8'h30) : (out_mat[2][15:12] + 8'h57) );
	assign sp_data[103:96] = ( (out_mat[2][11:8] < 4'hA) ? (out_mat[2][11:8] + 8'h30) : (out_mat[2][11:8] + 8'h57) );
	assign sp_data[111:104] = ( (out_mat[2][7:4] < 4'hA) ? (out_mat[2][7:4] + 8'h30) : (out_mat[2][7:4] + 8'h57) );
	assign sp_data[119:112] = ( (out_mat[2][3:0] < 4'hA) ? (out_mat[2][3:0] + 8'h30) : (out_mat[2][3:0] + 8'h57) );

	// /r/n
	assign sp_data[127:120] = 8'h0A;
	assign sp_data[135:128] = 8'h0D;
	
	// outmat[3]
	assign sp_data[143:136] = ( (out_mat[3][15:12] < 4'hA) ? (out_mat[3][15:12] + 8'h30) : (out_mat[3][15:12] + 8'h57) );
	assign sp_data[151:144] = ( (out_mat[3][11:8] < 4'hA) ? (out_mat[3][11:8] + 8'h30) : (out_mat[3][11:8] + 8'h57) );
	assign sp_data[159:152] = ( (out_mat[3][7:4] < 4'hA) ? (out_mat[3][7:4] + 8'h30) : (out_mat[3][7:4] + 8'h57) );
	assign sp_data[167:160] = ( (out_mat[3][3:0] < 4'hA) ? (out_mat[3][3:0] + 8'h30) : (out_mat[3][3:0] + 8'h57) );	
	
	// outmat[4]
	assign sp_data[175:168] = ( (out_mat[4][15:12] < 4'hA) ? (out_mat[4][15:12] + 8'h30) : (out_mat[4][15:12] + 8'h57) );
	assign sp_data[183:176] = ( (out_mat[4][11:8] < 4'hA) ? (out_mat[4][11:8] + 8'h30) : (out_mat[4][11:8] + 8'h57) );
	assign sp_data[191:184] = ( (out_mat[4][7:4] < 4'hA) ? (out_mat[4][7:4] + 8'h30) : (out_mat[4][7:4] + 8'h57) );
	assign sp_data[199:192] = ( (out_mat[4][3:0] < 4'hA) ? (out_mat[4][3:0] + 8'h30) : (out_mat[4][3:0] + 8'h57) );	
	
	// outmat[5]
	assign sp_data[207:200] = ( (out_mat[5][15:12] < 4'hA) ? (out_mat[5][15:12] + 8'h30) : (out_mat[5][15:12] + 8'h57) );
	assign sp_data[215:208] = ( (out_mat[5][11:8] < 4'hA) ? (out_mat[5][11:8] + 8'h30) : (out_mat[5][11:8] + 8'h57) );
	assign sp_data[223:216] = ( (out_mat[5][7:4] < 4'hA) ? (out_mat[5][7:4] + 8'h30) : (out_mat[5][7:4] + 8'h57) );
	assign sp_data[231:224] = ( (out_mat[5][3:0] < 4'hA) ? (out_mat[5][3:0] + 8'h30) : (out_mat[5][3:0] + 8'h57) );	
	
	// /r/n
	assign sp_data[239:232] = 8'h0A;
	assign sp_data[247:240] = 8'h0D;
	
	// outmat[6]
	assign sp_data[255:248] = ( (out_mat[6][15:12] < 4'hA) ? (out_mat[6][15:12] + 8'h30) : (out_mat[6][15:12] + 8'h57) );
	assign sp_data[263:256] = ( (out_mat[6][11:8] < 4'hA) ? (out_mat[6][11:8] + 8'h30) : (out_mat[6][11:8] + 8'h57) );
	assign sp_data[271:264] = ( (out_mat[6][7:4] < 4'hA) ? (out_mat[6][7:4] + 8'h30) : (out_mat[6][7:4] + 8'h57) );
	assign sp_data[279:272] = ( (out_mat[6][3:0] < 4'hA) ? (out_mat[6][3:0] + 8'h30) : (out_mat[6][3:0] + 8'h57) );	
	
	// outmat[7]
	assign sp_data[287:280] = ( (out_mat[7][15:12] < 4'hA) ? (out_mat[7][15:12] + 8'h30) : (out_mat[7][15:12] + 8'h57) );
	assign sp_data[291:288] = ( (out_mat[7][11:8] < 4'hA) ? (out_mat[7][11:8] + 8'h30) : (out_mat[7][11:8] + 8'h57) );
	assign sp_data[299:292] = ( (out_mat[7][7:4] < 4'hA) ? (out_mat[7][7:4] + 8'h30) : (out_mat[7][7:4] + 8'h57) );
	assign sp_data[307:300] = ( (out_mat[7][3:0] < 4'hA) ? (out_mat[7][3:0] + 8'h30) : (out_mat[7][3:0] + 8'h57) );	
	
	// outmat[7]
	assign sp_data[315:308] = ( (out_mat[8][15:12] < 4'hA) ? (out_mat[8][15:12] + 8'h30) : (out_mat[8][15:12] + 8'h57) );
	assign sp_data[323:316] = ( (out_mat[8][11:8] < 4'hA) ? (out_mat[8][11:8] + 8'h30) : (out_mat[8][11:8] + 8'h57) );
	assign sp_data[331:324] = ( (out_mat[8][7:4] < 4'hA) ? (out_mat[8][7:4] + 8'h30) : (out_mat[8][7:4] + 8'h57) );
	assign sp_data[339:332] = ( (out_mat[8][3:0] < 4'hA) ? (out_mat[8][3:0] + 8'h30) : (out_mat[8][3:0] + 8'h57) );
	
	// /r/n
	assign sp_data[347:340] = 8'h0A;
	assign sp_data[355:348] = 8'h0D;
	
	seq_printer sprinter (
		.clk(clk),
		.rst(rst),
		.en(sp_en),
		.keyboardclk(keyboardclk),
		.data_in(sp_data),
		.num_to_print(sp_num_to_print),
		.tx(sp_tx),
		.done(test)
	);


///////////////////////////////// 7 SEGMENT DISPLAY /////////////////////////////////

	// 7-segment clock interconnect
	wire segclk;

	// VGA display clock interconnect
	wire dclk;

	// disable the 7-segment decimal points
	assign dp = 1;

	// generate 7-segment clock & display clock
	clockdiv clk_divider(
		.clk(clk),
		.clr(clr),
		.segclk(segclk),
		.dclk(dclk)
	);

	// for debug
//	assign ssd_data = msel==4'b0 ? out_mat[0]
//						 : msel==4'b1 ? out_mat[1]
//						 : msel==4'b10 ? out_mat[2]
//						 : msel==4'b11 ? out_mat[3]
//						 : msel==4'b100 ? out_mat[4]
//						 : msel==4'b101 ? out_mat[5]
//						 : msel==4'b110 ? out_mat[6]
//						 : msel==4'b111 ? out_mat[7]
//						 : out_mat[8];

	// 7-segment display controller
	segdisplay ssd_displayer(
		.segclk	(segclk),
		.clr		(clr),
		.data		(out_mat[0]),
		.seg		(seg),
		.an		(an)
		);


///////////////////////////////// CMDPROCESSOR FSM /////////////////////////////////

	always @(negedge did_receive_kbd)
	begin
	
		// NOTE: it is assumed that there will be no keyboard inputs in the time that uart is printing to screen
		if (!rst)
		begin

			case (correct_led)
			
				// "i" has been pressed - instructions
				8'h69	:
				begin
					sp_en <= 0;
					mode <= 2'b00;
					pc <= imem_init_addr - 1;
					imem_addr <= imem_init_addr - 1;
				end
				
				// "l" has been pressed - alu
				8'h6C	:
				begin
					sp_en <= 0;
					mode <= 2'b01;
				end
				
				// "m" has been pressed - matrix multiply
				8'h6D	:
				begin
					sp_en <= 0;
					mode <= 2'b10;
					matmul_rst <= 1;	// clear data from last time
				end
				
				// "R" has been pressed
				8'h72	:
				begin
					ready_to_run <= 1'b1;
					matmul_en <= 1;		// start maccing. result will be ready in 3 clock cycles
				end
				
				// "x" has been pressed
				8'h78	:
				begin
					alu_op <= 4'b0111;
				end
				
				// "+" has been pressed
				8'h2B	:
				begin
					alu_op <= 4'b0101;
				end
				
				// "-" has been pressed
				8'h2D	:
				begin
					alu_op <= 4'b0110;
				end
				
				// "*" has been pressed
				8'h2A	:
				begin
					alu_op <= 4'b1111;
				end
				
				// "ENTER" has been presesd
				8'h0A	:
				begin
					
					case (mode)
						
						// if R was pressed before this, run all instr. Else add instr to mem
						2'b00:
						begin
							
							if (ready_to_run == 1'b1)
							begin
								// start processor
								sp_en <= 1;
								ready_to_run <= 0;
								num_data_rec <= 0;
								imem_write_en <= 0;
								imem_output_en <= 1;
							end
							
							// start writing to mem at the enter pressed after i->ENTER
							else
							begin

								if (num_data_rec != 0)
								begin
									imem_write_en <= 1;
									imem_write_data <= { data_buf[0],data_buf[1],data_buf[2],data_buf[3] };
									imem_addr <= imem_addr + 1'b1;
								end

								num_data_rec <= num_data_rec + 1;

							end
						
						end // 2'b00

						// run the alu. result will be available in next cycle
						2'b01:
						begin
							if (num_data_rec == 2'b10)	// todo: prevents empty enter keys from executing alu output
							begin
							
								operandB <= { data_buf[0],data_buf[1],data_buf[2],data_buf[3] };
								num_data_rec <= 0;
								sp_en <= 1;

							end
						end

						// if R was pressed before this, run matmul. Else keep receiving input data
						2'b10:
						begin
							
							// run matmul
							if (ready_to_run == 1'b1)
							begin
								sp_en <= 1;
//								matmul_en <= 0;	// running started when r was pressed. now stop running.
								in_mat_idx <= 0;
								ready_to_run <= 0;
								num_data_rec <= 0;
							end
							
							// update matrix entries at the ENTER after b->ENTER
							else
							begin

								if (num_data_rec != 0)
								begin
									in_mat[in_mat_idx] <= { data_buf[0],data_buf[1],data_buf[2],data_buf[3] };
									in_mat_idx <= in_mat_idx + 1'b1;
									num_data_rec <= num_data_rec + 1;
								end

								num_data_rec <= num_data_rec + 1'b1;
								
							end
						
						end

					endcase // mode

					// clear the buffer no matter what
					data_buf[3]  <= 4'b0;
					data_buf[2]  <= 4'b0;
					data_buf[1]  <= 4'b0;
					data_buf[0]	 <= 4'b0;
					
					// reset buf idx
					buf_idx <= 2'b00;
					
				end // ENTER

				// "SPACE" has been pressed
				8'h20	:	// update data value. clear buffer. increment dat aintput counter
				begin

					case (mode)
						
						// if it's the first space, store value as operandA, op will be defined on keypress, operandB will be defined on ENTER
						2'b01:
						begin
							if (num_data_rec == 0)
							begin
								operandA <= { data_buf[0],data_buf[1],data_buf[2],data_buf[3] };
							end
						end
						
						// update matrix entries
						2'b10:
						begin
							matmul_rst <= 0;
							in_mat[in_mat_idx] <= { data_buf[0],data_buf[1],data_buf[2],data_buf[3] };
							in_mat_idx <= in_mat_idx + 1'b1;
						end

					endcase // mode

					// update number of data received
					num_data_rec <= num_data_rec + 1'b1;
					
					// clear the buffer no matter what
					data_buf[3]  <= 4'b0;
					data_buf[2]  <= 4'b0;
					data_buf[1]  <= 4'b0;
					data_buf[0]	 <= 4'b0;
					
					// reset buf idx
					buf_idx <= 2'b00;

				end // SPACE

				// decode input and add to input buffer
				default	:
				begin
					data_buf[buf_idx] <= ( (correct_led < 8'h3A) ? (correct_led - 8'h30) : (correct_led - 8'h57) );
					buf_idx <= buf_idx + 1'b1;
					imem_write_en <= 0;
					sp_en <= 0;
				end

			endcase // correct_led
	
		end // rst

	end	// always did_receive_kbd

endmodule
