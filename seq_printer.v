`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    16:05:55 04/15/2018 
// Design Name: 
// Module Name:    seq_printer 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module seq_printer(
	input 							clk,
	input 							rst,
	input 							en,
	input 							keyboardclk,
	input		[6:0]					num_to_print,
	input		[355:0]				data_in,
	output 							tx,
	output reg						done = 0			// done or not
    );
	 
	reg			transmit_it;
	wire 			is_transmitting;
	reg [6:0] 	num_printed;
	reg [7:0] 	tx_data;
	reg [9:0] 	sindex;
	
	uart instance3(
		.uart_busy (is_transmitting),
		.uart_tx (tx),
		.uart_wr_i(transmit_it),
		.uart_dat_i (tx_data),
		.sys_clk_i (clk), 
		.sys_rst_i (rst)
	);
	
	reg [31:0] db_counter = 0;
	parameter integer MAX = 100000;	// 0.1s
	
	always @ (posedge clk)
	begin
		
		if (rst==1 || en==0)
		begin
			transmit_it <= 0;
			db_counter <= 0;
			sindex <= 7;
			num_printed <= 0;
			done <= 0;
		end // rst


		else if (en==1)
		begin
		
			// debouncing not finished
			if (db_counter != MAX)
			begin
				transmit_it <= 0;
				db_counter <= db_counter + 1;
			end
			
			// debouncing finished
			else
			begin
			
				if (is_transmitting==0)
				begin
					if (num_printed != num_to_print)
					begin
					
						case (num_printed)
							0: tx_data <= data_in[7:0];
							1: tx_data <= data_in[15:8];
							2: tx_data <= data_in[23:16];
							3: tx_data <= data_in[31:24];
							4: tx_data <= data_in[39:32];
							5: tx_data <= data_in[47:40];
							6: tx_data <= data_in[55:48];
							default: tx_data <= data_in[63:56];
						endcase
						
						transmit_it <= 1;
						db_counter <= 0;
						num_printed <= num_printed + 1;
					end
					else
					begin
						done <= 1;
						transmit_it <= 0;
					end
					
				end
				
			end
		
		end // en
		
	end	// always clk
	
	
//	wire myclk;
//	debouncer db(.clk(clk), .I(keyboardclk), .O(myclk));
//	
//	always @ (negedge is_transmitting)
//	begin
//
//		if (rst || !en)
//		begin
//			tx_data <= 0;
//			sindex <= 7;
//			transmit_it <= 0;
//			num_printed <= 0;
//		end
//
//		else if (en)
//		begin
//		
//			done <= !done;
//		
//			if ( num_printed != num_to_print )
//			begin
//			
//				// print the required data
//				case (sindex)
//					8'd7:		tx_data <= data_in[7:0];
//					8'd15: 	tx_data <= data_in[15:8];
//					8'd23:	tx_data <= data_in[23:16];
//					8'd31:	tx_data <= data_in[31:24];
//					default 	tx_data <= 8'h0;
//				endcase
//				
//				// commence printing
//				transmit_it <= 1;
//				
//				// update for next iter
//				sindex <= sindex + 4'b1000;
//				num_printed <= num_printed + 1;
//			
//			end
//			
//			else
//			begin
//				transmit_it <= 0;
//			end
//		
//		end // en
//
//	end // myclk

endmodule
