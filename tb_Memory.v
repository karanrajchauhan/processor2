`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer:
//
// Create Date:   18:54:51 04/11/2018
// Design Name:
// Module Name:   tb_Memory.v
// Project Name:
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture created by ISE for module: Memory
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////

module tb_Memory;

	// Inputs
	reg clk;
	reg rst;
	reg write_en;
	reg output_en;
	reg [3:0] address;
	reg [15:0] write_data;

	// Outputs
	wire [15:0] read_data;

	// Instantiate the Unit Under Test (UUT)
	Memory uut (
		.clk (clk), 
		.rst (rst),
		.output_en (output_en), 
		.write_en (write_en),
		.address (address),
		.write_data (write_data),
		.read_data (read_data)
	);

	always #1 clk = !clk;

	initial begin

		$dumpfile("Memory.vcd");
        $dumpvars(clk, rst, uut);

		// Initialize Inputs
		clk = 1;
		rst = 1;
		write_en = 0;
		address = 0;
		write_data = 0;
        output_en = 0;
	
		// Wait 100 ns for global reset to finish
		#50 rst = 0;
        
        // read from mem[0]. try writing, should not work
        output_en = 1;
        write_en = 0;
        write_data = 1;
        #4;

        // enable write
		write_en = 1;
        #4;

        // read freom mem[1]
        write_en = 0;  
        address = 1;
        #4;

        // read freom mem[0]
        address = 0;
        #4;

		$finish;

	end
      
endmodule

