`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer:
//
// Create Date:   17:57:54 03/04/2018
// Design Name:   RegisterFile
// Module Name:   X:/Documents/Processor/tb_RegisterFile.v
// Project Name:  Processor
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture created by ISE for module: RegisterFile
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////

module tb_RegisterFile;

	// Inputs
	reg 		clk;
	reg 		rst;
	reg 		write_en;
	reg [3:0]	instr_opcode;
	reg [5:0] 	read1_sel;
	reg [5:0] 	read2_sel;
	reg [5:0] 	write_sel;
	reg [15:0]	write_data;

	// Outputs
	wire [15:0] read1_data;
	wire [15:0] read2_data;

	// Instantiate the Unit Under Test (UUT)
	RegisterFile uut (
		.clk	     (clk), 
		.rst	     (rst), 
		.write_en	 (write_en),
		.instr_opcode(instr_opcode),
		.read1_sel   (read1_sel), 
		.read2_sel   (read2_sel), 
		.write_sel   (write_sel), 
		.write_data  (write_data), 
		.read1_data  (read1_data), 
		.read2_data  (read2_data)
	);


	always #1 clk = !clk;

	initial begin
		
		$dumpfile("registerfile.vcd");
		$dumpvars(clk, rst, write_en, instr_opcode, read1_sel, read2_sel, write_sel, write_data, uut);

		// Initialize Inputs
		clk = 1;
		rst = 1;
		write_en = 0;
		read1_sel = 0;
		read2_sel = 0;
		write_sel = 0;
		write_data = 0;

		// Wait 100 ns for global reset to finish
		#100 rst = 0;
		
		// should not write because write_en=0
		//repeat (1) @ (posedge clk);
		#2 read1_sel = 6'b1;
		read2_sel = 6'b10;
		write_sel = 6'b1;
		write_data = 16'b10;
		//$display("Read1Reg=%b Read2Reg=%b Read1Data=%b Read2Data=%b WriteReg=%b WriteData=%b WriteEn=%b ", read1_sel, read2_sel, read1_data, read2_data, write_sel, write_data, write_en);
//
		// should write 2 to reg 1. reads should still be 0
		//repeat (1) @ (posedge clk);
		#2 read1_sel = 6'b1;
		read2_sel = 6'b10;
		write_en = 1;
		write_sel = 6'b1;
		write_data = 16'b10;
		//$display("Read1Reg=%b Read2Reg=%b Read1Data=%b Read2Data=%b WriteReg=%b WriteData=%b WriteEn=%b ", read1_sel, read2_sel, read1_data, read2_data, write_sel, write_data, write_en);

		// should write 4 to reg 2. reads should still be 0
		//repeat (1) @ (posedge clk);
		#2 read1_sel = 6'b1;
		read2_sel = 6'b10;
		write_en = 1;
		write_sel = 6'b10;
		write_data = 16'b100;
		//$display("Read1Reg=%b Read2Reg=%b Read1Data=%b Read2Data=%b WriteReg=%b WriteData=%b WriteEn=%b ", read1_sel, read2_sel, read1_data, read2_data, write_sel, write_data, write_en);

		// should read1=reg1=2, read2=reg2=4
		//repeat (1) @ (posedge clk);
		#2 read1_sel = 6'b1;
		read2_sel = 6'b10;
		write_en = 0;
		write_sel = 6'b1;
		write_data = 16'b10;
		//$display("Read1Reg=%b Read2Reg=%b Read1Data=%b Read2Data=%b WriteReg=%b WriteData=%b WriteEn=%b ", read1_sel, read2_sel, read1_data, read2_data, write_sel, write_data, write_en);

		// should read1=reg2=4, read2=reg1=2
		//repeat (1) @ (posedge clk);
		#2 read1_sel = 6'b10;
		read2_sel = 6'b1;
		write_en = 0;
		write_sel = 6'b1;
		write_data = 16'b10;
		//$display("Read1Reg=%b Read2Reg=%b Read1Data=%b Read2Data=%b WriteReg=%b WriteData=%b WriteEn=%b ", read1_sel, read2_sel, read1_data, read2_data, write_sel, write_data, write_en);

		#4 $finish;

	end
      
endmodule

