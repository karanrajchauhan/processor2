`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer:
//
// Create Date:   20:05:38 04/16/2018
// Design Name:   key2ascii
// Module Name:   X:/Documents/Processor2/tb_func.v
// Project Name:  Processor2
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture created by ISE for module: key2ascii
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////

//module traffic_lights;
//	reg [3:0] foo = 4'b0101;
//	reg [7:0] bar;
//	reg clk;
//
//	always #1 clk = !clk;
//	
//	// sequence to control the lights
//	always begin
//		light(bar, foo); // and wait.
//	end
//	
//	// task to wait for 'tics' positive edge clocks
//	// before turning 'color' light off
//	task light;
//		output	[7:0] myout;
//		input		[3:0] myin;
//		input				clk;
//		begin
//			always @ (posedge clk)
//			begin
//				myout = 8'b00110011;
//			end
//		end
//	endtask
//	
//endmodule // traffic_lights

