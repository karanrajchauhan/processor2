`timescale 1ns / 1ps

module tb_CPU;

	// Inputs
	reg clk;
	reg rst;
	reg en;

	// Outputs
	wire done;
	wire halt;
	wire glb_overflow;
	wire glb_is_equal;

	CPU uut (
		.clk         (clk),
		.rst         (rst),
		.en          (en),
		.halt			 (halt),
		// .done        (done),
		.glb_overflow(glb_overflow),
		.glb_is_equal(glb_is_equal)
	);

	always #1 clk = !clk;

	initial
	begin

//		$dumpfile("CPU.vcd");
//		$dumpvars(clk, rst, uut);

		clk = 1;
		rst = 1;
		en = 0;

		#50 rst = 0;
		uut.reg_file.registers[0] = 16'b1;
		uut.dmem.memory[0] = 16'b1001;
		// en = 1;

		#2 en = 1;
		#50;

//		$finish;

	end

endmodule