`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    18:44:46 04/11/2018
// Design Name: 
// Module Name:    Memory 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module Memory #(parameter integer LOG_MEM_SIZE = 4,
	parameter integer DATA_WIDTH = 16)(
	input 									clk,
	input 									rst,
	input										output_en,
	input 									write_en,
	input			[LOG_MEM_SIZE-1:0]	address,
	input			[DATA_WIDTH-1:0] 		write_data,
	output reg	[DATA_WIDTH-1:0] 		read_data
);

	reg [DATA_WIDTH-1:0] memory[0:2**LOG_MEM_SIZE-1];

	reg [DATA_WIDTH-1:0] i;
	always @ (posedge clk)
	begin
	
		// reset to 0's if rst is asserted
		if (rst)
		begin
			for (i=0; i<(2**LOG_MEM_SIZE); i=i+1)
				memory[i] <= 32'b0;
		end
		
		// if write_en is asserted then write to mem o/w read
		else
		begin
			if (write_en == 1'b1)
			begin
				memory[address] <= write_data;
			end
			else if (output_en == 1'b1)
			begin
				read_data <= memory[address];
			end
		end
	
	end // clk

endmodule // Memory
