`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer:
//
// Create Date:   17:58:35 04/01/2018
// Design Name:   FactorialCalculator
// Module Name:   X:/Documents/Processor2/tb_FactorialCalculator.v
// Project Name:  Processor2
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture created by ISE for module: FactorialCalculator
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////

module tb_FactorialCalculator;

	// Inputs
	reg clk;
	reg rst;
	reg [3:0] data;

	// Outputs
	wire overflow;
	wire [31:0] result;

	// Instantiate the Unit Under Test (UUT)
	FactorialCalculator uut (
		.clk(clk), 
		.rst(rst), 
		.data(data), 
		.overflow(overflow), 
		.result(result)
	);
	
	always #1 clk = !clk;

	initial begin
		// Initialize Inputs
		clk = 0;
		rst = 1;
		data = 0;

		// Wait 100 ns for global reset to finish
		#100;
		rst = 0;
		
		data = 4'b1010;
        
	end
      
endmodule

