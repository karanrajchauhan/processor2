`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer:
//
// Create Date:   19:49:38 04/01/2018
// Design Name:   MatMul
// Module Name:   X:/Documents/Processor2/tb_MatMul.v
// Project Name:  Processor2
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture created by ISE for module: MatMul
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////

module tb_MatMul;

	// Inputs
	reg clk;
	reg rst;
	reg en;
	reg [17:0] a0;
	reg [17:0] a1;
	reg [17:0] a2;
	reg [17:0] a3;
	reg [17:0] a4;
	reg [17:0] a5;
	reg [17:0] a6;
	reg [17:0] a7;
	reg [17:0] a8;
	reg [17:0] b0;
	reg [17:0] b1;
	reg [17:0] b2;
	reg [17:0] b3;
	reg [17:0] b4;
	reg [17:0] b5;
	reg [17:0] b6;
	reg [17:0] b7;
	reg [17:0] b8;
	
	// Outputs
	wire done;
	wire [17:0] c0;
	wire [17:0] c1;
	wire [17:0] c2;
	wire [17:0] c3;
	wire [17:0] c4;
	wire [17:0] c5;
	wire [17:0] c6;
	wire [17:0] c7;
	wire [17:0] c8;

	// Instantiate the Unit Under Test (UUT)
	MatMul uut (
		.clk(clk), 
		.rst(rst),
		.en(en),
		.a0(a0), 
		.a1(a1), 
		.a2(a2), 
		.a3(a3), 
		.a4(a4), 
		.a5(a5), 
		.a6(a6), 
		.a7(a7), 
		.a8(a8), 
		.b0(b0), 
		.b1(b1), 
		.b2(b2), 
		.b3(b3), 
		.b4(b4), 
		.b5(b5), 
		.b6(b6), 
		.b7(b7), 
		.b8(b8), 
		.c0(c0),
		.c1(c1), 
		.c2(c2), 
		.c3(c3), 
		.c4(c4), 
		.c5(c5), 
		.c6(c6), 
		.c7(c7), 
		.c8(c8),
		.done(done)
	);

	always #1 clk = !clk;

	initial begin
		// Initialize Inputs
		clk = 1;
		rst = 1;
		en = 0;
		a1 = 1;
		a2 = 1;
		a3 = 1;
		a4 = 1;
		a5 = 1;
		a6 = 1;
		a7 = 1;
		a8 = 1;
		a0 = 1;
		b1 = 16'h11;
		b2 = 16'h11;
		b3 = 16'h11;
		b4 = 16'h11;
		b5 = 16'h11;
		b6 = 16'h11;
		b7 = 16'h11;
		b8 = 16'h11;
		b0 = 16'h11;

		// Wait 100 ns for global reset to finish
		#100 rst = 0;
//		en = 1;
		
		#4 en = !rst && !done;
//		#8 en = 0;
        
	end
      
endmodule

