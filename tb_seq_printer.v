`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer:
//
// Create Date:   16:19:10 04/15/2018
// Design Name:   seq_printer
// Module Name:   X:/Desktop/LAB_2/Lab2/processor2/tb_seq_printer.v
// Project Name:  Processor2
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture created by ISE for module: seq_printer
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////

module tb_seq_printer;

	// Inputs
	reg clk;
	reg rst;
	reg transmit_it;
	reg keyboardclk;

	// Outputs
	wire tx;

	// Instantiate the Unit Under Test (UUT)
	seq_printer uut (
		.clk(clk), 
		.rst(rst), 
		.transmit_it(transmit_it), 
		.keyboardclk(keyboardclk), 
		.tx(tx)
	);
	always #1 clk =~ clk;
	always #10 keyboardclk =~ keyboardclk;
	initial begin
		// Initialize Inputs
		clk = 0;
		rst = 1;
		transmit_it = 0;
		keyboardclk = 0;
		uut.dummy_data[0] = 8'h1C;
		uut.dummy_data[1] = 8'h32;
		uut.dummy_data[2] = 8'h22;
		uut.index = 0;
//		uut.data_in
		// Wait 100 ns for global reset to finish
		#100;
      rst = 0;
		// Add stimulus here
		
	end
      
endmodule

