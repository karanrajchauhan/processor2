`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    19:37:32 03/03/2018 
// Design Name: 
// Module Name:    RegisterFile 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module RegisterFile(
	input 				clk,
	input 				rst,
	input 				write_en,
	input 		[3:0] 	instr_opcode,
	input 		[5:0] 	read1_sel,
	input 		[5:0] 	read2_sel,
	input 		[5:0] 	write_sel,
	input 		[15:0] 	write_data,
	output 		[15:0] 	read1_data,
	output	 	[15:0] 	read2_data
    );
	 
	reg [15:0] registers[0:5];
	 
	// always @ (*)
	always @ (posedge clk)
	begin

		// reset all registers to 0
		if (rst == 1)
		begin
			registers[0] <= 16'b0;
			registers[1] <= 16'b0;
			registers[2] <= 16'b0;
			registers[3] <= 16'b0;
			registers[4] <= 16'b0;
			registers[5] <= 16'b0;
//			read1_data <= 16'b0;
//			read2_data <= 16'b0;	
		end

		else
		begin

			// write data into a register but only if in writeback stage
			if (write_en == 1)
			begin
				registers[write_sel] <= write_data;
			end
			
			// read data from a register

//			// mov Rn, num
//			if (instr_opcode == 4'b1001)
//			begin
//				read1_data <= registers[read1_sel];
//				read2_data <= read2_sel;
//			end
//
//			// halt, jump, jne, je -- MIGHT have invalid outputs, just put 0 to be safe
//			else if ( (instr_opcode != 4'b0001) && (instr_opcode < 4'b0101) )
//			begin
//				read1_data <= 16'b0;
//				read2_data <= 16'b0;	
//			end
//
//			else
//			begin
//				read1_data <= registers[read1_sel];
//				read2_data <= registers[read2_sel];
//			end
			
		end

	end
	
	// combinational reading
	assign read1_data = ( (instr_opcode != 4'b0001) && (instr_opcode < 4'b0101) ) ? 16'b0	// halt, jump, jne, je -- MIGHT have invalid outputs, just put 0 to be safe
							: registers[read1_sel];																// in all other cases just read value at sel
	
	
	assign read2_data = (instr_opcode == 4'b1001) ? read2_sel										// mov Rn, num
							: ( (instr_opcode != 4'b0001) && (instr_opcode < 4'b0101) ) ? 16'b0	// halt, jump, jne, je -- MIGHT have invalid outputs, just put 0 to be safe
							: registers[read2_sel];																// in all other cases just read value at sel

	
endmodule
