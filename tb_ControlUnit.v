`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer:
//
// Create Date:   16:27:37 03/04/2018
// Design Name:   ControlUnit
// Module Name:   X:/Documents/Processor/tb_ControlUnit.v
// Project Name:  Processor
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture created by ISE for module: ControlUnit
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////

module tb_ControlUnit;

	// Inputs
	reg clk;
	reg rst;
	reg glb_is_equal;
	reg [3:0] instr_opcode;

	// Outputs
	wire halt;
	wire jump;
	wire reg_write;
	wire mem_write;
	wire reg_write_data_sel;

	// Instantiate the Unit Under Test (UUT)
	ControlUnit uut (
		.clk               (clk),
		.rst               (rst),
		.glb_is_equal      (glb_is_equal),
		.instr_opcode      (instr_opcode),
		.halt              (halt),
		.jump              (jump),
		.reg_write         (reg_write),
		.mem_write         (mem_write),
		.reg_write_data_sel(reg_write_data_sel)
	);
	
	always #1 clk = ~clk;

	initial begin
		$dumpfile("controlunit.vcd");
		$dumpvars(clk, instr_opcode, uut);

		// Initialize Inputs
		rst = 1;
		clk = 0;
		glb_is_equal = 0;
		instr_opcode = 0;
		
		// Wait 100 ns for global reset to finish
		#100 rst = 0;
       
		// halt
		//repeat (1) @ (posedge clk);
		#2 instr_opcode = 4'b0000;
		//$display("InstrOp=%b Halt=%b Jump=%b MemWrite=%b MemRea%b AluCtrl=%b", instr_opcode, halt, jump, mem_write, mem_read, alu_ctrl);

		// inc
		//repeat (1) @ (posedge clk);
		#2 instr_opcode = 4'b0001;
		//$display("InstrOp=%b Halt=%b Jump=%b MemWrite=%b MemRea%b AluCtrl=%b", instr_opcode, halt, jump, mem_write, mem_read, alu_ctrl);

		// jmp
		//repeat (1) @ (posedge clk);
		#2 instr_opcode = 4'b0010;
		//$display("InstrOp=%b Halt=%b Jump=%b MemWrite=%b MemRea%b AluCtrl=%b", instr_opcode, halt, jump, mem_write, mem_read, alu_ctrl);

		// jne
		//repeat (1) @ (posedge clk);
		#2 instr_opcode = 4'b0011;
		//$display("InstrOp=%b Halt=%b Jump=%b MemWrite=%b MemRea%b AluCtrl=%b", instr_opcode, halt, jump, mem_write, mem_read, alu_ctrl);

		// je
		//repeat (1) @ (posedge clk);
		#2 instr_opcode = 4'b0100;
		//$display("InstrOp=%b Halt=%b Jump=%b MemWrite=%b MemRea%b AluCtrl=%b", instr_opcode, halt, jump, mem_write, mem_read, alu_ctrl);

		// add
		//repeat (1) @ (posedge clk);
		#2 instr_opcode = 4'b0101;
		//$display("InstrOp=%b Halt=%b Jump=%b MemWrite=%b MemRea%b AluCtrl=%b", instr_opcode, halt, jump, mem_write, mem_read, alu_ctrl);

		// sub
		//repeat (1) @ (posedge clk);
		#2 instr_opcode = 4'b0110;
		//$display("InstrOp=%b Halt=%b Jump=%b MemWrite=%b MemRea%b AluCtrl=%b", instr_opcode, halt, jump, mem_write, mem_read, alu_ctrl);
		
		
		// xor
		//repeat (1) @ (posedge clk);
		#2 instr_opcode = 4'b0111;
		//$display("InstrOp=%b Halt=%b Jump=%b MemWrite=%b MemRea%b AluCtrl=%b", instr_opcode, halt, jump, mem_write, mem_read, alu_ctrl);
		
		// cmp
		//repeat (1) @ (posedge clk);
		#2 instr_opcode = 4'b1000;
		//$display("InstrOp=%b Halt=%b Jump=%b MemWrite=%b MemRea%b AluCtrl=%b", instr_opcode, halt, jump, mem_write, mem_read, alu_ctrl);
		
		// special isntruction
		//repeat (1) @ (posedge clk);
		#2 instr_opcode = 4'b1101;
		//$display("InstrOp=%b Halt=%b Jump=%b MemWrite=%b MemRea%b AluCtrl=%b", instr_opcode, halt, jump, mem_write, mem_read, alu_ctrl);
		
		// mov rn, num
		//repeat (1) @ (posedge clk);
		#2 instr_opcode = 4'b1001;
		//$display("InstrOp=%b Halt=%b Jump=%b MemWrite=%b MemRea%b AluCtrl=%b", instr_opcode, halt, jump, mem_write, mem_read, alu_ctrl);
		
		// mov rn, rm
		//repeat (1) @ (posedge clk);
		#2 instr_opcode = 4'b1010;
		//$display("InstrOp=%b Halt=%b Jump=%b MemWrite=%b MemRea%b AluCtrl=%b", instr_opcode, halt, jump, mem_write, mem_read, alu_ctrl);
		
		// sw mov [rn], rm
		//repeat (1) @ (posedge clk);
		#2 instr_opcode = 4'b1011;
		//$display("InstrOp=%b Halt=%b Jump=%b MemWrite=%b MemRea%b AluCtrl=%b", instr_opcode, halt, jump, mem_write, mem_read, alu_ctrl);
		
		// lw mov rn, [rm]
		//repeat (1) @ (posedge clk);
		#2 instr_opcode = 4'b1100;
		//$display("InstrOp=%b Halt=%b Jump=%b MemWrite=%b MemRea%b AluCtrl=%b", instr_opcode, halt, jump, mem_write, mem_read, alu_ctrl);


		#100 $finish;
	end
      
endmodule

