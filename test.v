module test;

	reg clk;
	reg [15:0] foo = 0;
	reg [15:0] counter = 0;

	always #1 clk <= !clk;
	always #5 foo <= foo;

	always @ (foo)
	begin
		counter <= counter + 1'b1;
	end

	initial
	begin
		$dumpfile("test.vcd");
		$dumpvars(clk, foo, counter);
		#100;
		$finish;
	end

endmodule