`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: assign 
// Engineer: assign 
// 
// Create Date: assign    19:59:41 04/11/2018 
// Design Name: assign 
// Module Name: assign    ALU 
// Project Name: assign 
// Target Devices: assign 
// Tool versions: assign 
// Description: assign 
//
// Dependencies: assign 
//
// Revision: assign 
// Revision 0.01 - File Created
// Additional Comments: assign 
//
//////////////////////////////////////////////////////////////////////////////////
module ALU #(parameter integer DATA_WIDTH = 16)(
	input 								clk,
	input 								rst,
	input	 		[3:0]					alu_op,
	input 		[DATA_WIDTH-1:0] 	operand_A,
	input 		[DATA_WIDTH-1:0] 	operand_B,
	output reg 							is_equal,
	output reg 	[DATA_WIDTH-1:0]	result
);

	always @ (*)
	begin

		if (!rst)
		begin

			case (alu_op)

				// add
				4'b0101:	result <= operand_A + operand_B;

				// sub
				4'b0110:	result <= operand_A - operand_B;

				// xor
				4'b0111:	result <= operand_A ^ operand_B;

				// special: multiply
				4'b1111:	result <= operand_A * operand_B;

				// jne
				4'b0011:	is_equal <= operand_A==operand_B;
				
				// je
				4'b0100:	is_equal <= operand_A==operand_B;

				// cmp
				4'b1000:	is_equal <= operand_A==operand_B;

				// default cases use operand b directly
				default: 	result <= operand_B;
		
			endcase // alu_op

		end // rst
	
	end // clk

endmodule // ALU
