`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    19:41:05 04/01/2018 
// Design Name: 
// Module Name:    MatMul 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////

/*********************************************************************************
 * TODO
 * 0. !!! MUST RST MATMUL FROM TOPMODULE AFTER RESULT IS RECEIVED !!!
 * 1. make input width variable
 * 2. make matrix size variable
 *********************************************************************************/
module MatMul #(
	parameter integer DATA_WIDTH = 16,
	parameter integer NUM_CYCLES_REQ = 3)(
	
	input clk,
	input rst,
	input en,
	
	input [DATA_WIDTH-1:0] a0,
	input [DATA_WIDTH-1:0] a1,
	input [DATA_WIDTH-1:0] a2,
	input [DATA_WIDTH-1:0] a3,
	input [DATA_WIDTH-1:0] a4,
	input [DATA_WIDTH-1:0] a5,
	input [DATA_WIDTH-1:0] a6,
	input [DATA_WIDTH-1:0] a7,
	input [DATA_WIDTH-1:0] a8,
	
	input [DATA_WIDTH-1:0] b0,
	input [DATA_WIDTH-1:0] b1,
	input [DATA_WIDTH-1:0] b2,
	input [DATA_WIDTH-1:0] b3,
	input [DATA_WIDTH-1:0] b4,
	input [DATA_WIDTH-1:0] b5,
	input [DATA_WIDTH-1:0] b6,
	input [DATA_WIDTH-1:0] b7,
	input [DATA_WIDTH-1:0] b8,
	
	output reg done,
	output [2*DATA_WIDTH-1:0] c0,
	output [2*DATA_WIDTH-1:0] c1,
	output [2*DATA_WIDTH-1:0] c2,
	output [2*DATA_WIDTH-1:0] c3,
	output [2*DATA_WIDTH-1:0] c4,
	output [2*DATA_WIDTH-1:0] c5,
	output [2*DATA_WIDTH-1:0] c6,
	output [2*DATA_WIDTH-1:0] c7,
	output [2*DATA_WIDTH-1:0] c8

	);
	
	// enable only when input en is high and calc is not finished
	
	// how many cycles have been spent MACC'ing
	reg [1:0] num_cycles_ran;
	
	// form matrices from inputs
	wire [DATA_WIDTH-1:0] amat[0:2][0:2];
	wire [DATA_WIDTH-1:0] bmat[0:2][0:2];

	assign amat[0][0] = a0;
	assign amat[0][1] = a1;
	assign amat[0][2] = a2;
	assign amat[1][0] = a3;
	assign amat[1][1] = a4;
	assign amat[1][2] = a5;
	assign amat[2][0] = a6;
	assign amat[2][1] = a7;
	assign amat[2][2] = a8;
	
	assign bmat[0][0] = b0;
	assign bmat[0][1] = b1;
	assign bmat[0][2] = b2;
	assign bmat[1][0] = b3;
	assign bmat[1][1] = b4;
	assign bmat[1][2] = b5;
	assign bmat[2][0] = b6;
	assign bmat[2][1] = b7;
	assign bmat[2][2] = b8;

	// indices into a and b matrices
	reg [1:0] idx = 2'b11;
	
	MACC_MACRO #(
		.DEVICE("SPARTAN6"),
		.LATENCY(1),
		.WIDTH_A(DATA_WIDTH),
		.WIDTH_B(DATA_WIDTH),
		.WIDTH_P(2*DATA_WIDTH)
	) macc0 (
		.P(c0),
		.A(amat[0][idx]),
		.ADDSUB(1'b1),
		.B(bmat[idx][0]),
		.CARRYIN(1'b0),
		.CE(en && !done),
		.CLK(clk),
		.LOAD(1'b0),
		.LOAD_DATA(32'b0),
		.RST(rst)
	);

	MACC_MACRO #(
		.DEVICE("SPARTAN6"),
		.LATENCY(1),
		.WIDTH_A(DATA_WIDTH),
		.WIDTH_B(DATA_WIDTH),
		.WIDTH_P(2*DATA_WIDTH)
	) macc1 (
		.P(c1),
		.A(amat[0][idx]),
		.ADDSUB(1'b1),
		.B(bmat[idx][1]),
		.CARRYIN(1'b0),
		.CE(en && !done),
		.CLK(clk),
		.LOAD(1'b0),
		.LOAD_DATA(32'b0),
		.RST(rst)
	);
	
	MACC_MACRO #(
		.DEVICE("SPARTAN6"),
		.LATENCY(1),
		.WIDTH_A(DATA_WIDTH),
		.WIDTH_B(DATA_WIDTH),
		.WIDTH_P(2*DATA_WIDTH)
	) macc2 (
		.P(c2),
		.A(amat[0][idx]),
		.ADDSUB(1'b1),
		.B(bmat[idx][2]),
		.CARRYIN(1'b0),
		.CE(en && !done),
		.CLK(clk),
		.LOAD(1'b0),
		.LOAD_DATA(32'b0),
		.RST(rst)
	);
	
	MACC_MACRO #(
		.DEVICE("SPARTAN6"),
		.LATENCY(1),
		.WIDTH_A(DATA_WIDTH),
		.WIDTH_B(DATA_WIDTH),
		.WIDTH_P(2*DATA_WIDTH)
	) macc3 (
		.P(c3),
		.A(amat[1][idx]),
		.ADDSUB(1'b1),
		.B(bmat[idx][0]),
		.CARRYIN(1'b0),
		.CE(en && !done),
		.CLK(clk),
		.LOAD(1'b0),
		.LOAD_DATA(32'b0),
		.RST(rst)
	);
	
	MACC_MACRO #(
		.DEVICE("SPARTAN6"),
		.LATENCY(1),
		.WIDTH_A(DATA_WIDTH),
		.WIDTH_B(DATA_WIDTH),
		.WIDTH_P(2*DATA_WIDTH)
	) macc4 (
		.P(c4),
		.A(amat[1][idx]),
		.ADDSUB(1'b1),
		.B(bmat[idx][1]),
		.CARRYIN(1'b0),
		.CE(en && !done),
		.CLK(clk),
		.LOAD(1'b0),
		.LOAD_DATA(32'b0),
		.RST(rst)
	);
	
	MACC_MACRO #(
		.DEVICE("SPARTAN6"),
		.LATENCY(1),
		.WIDTH_A(DATA_WIDTH),
		.WIDTH_B(DATA_WIDTH),
		.WIDTH_P(2*DATA_WIDTH)
	) macc5 (
		.P(c5),
		.A(amat[1][idx]),
		.ADDSUB(1'b1),
		.B(bmat[idx][2]),
		.CARRYIN(1'b0),
		.CE(en && !done),
		.CLK(clk),
		.LOAD(1'b0),
		.LOAD_DATA(32'b0),
		.RST(rst)
	);
	
	MACC_MACRO #(
		.DEVICE("SPARTAN6"),
		.LATENCY(1),
		.WIDTH_A(DATA_WIDTH),
		.WIDTH_B(DATA_WIDTH),
		.WIDTH_P(2*DATA_WIDTH)
	) macc6 (
		.P(c6),
		.A(amat[2][idx]),
		.ADDSUB(1'b1),
		.B(bmat[idx][0]),
		.CARRYIN(1'b0),
		.CE(en && !done),
		.CLK(clk),
		.LOAD(1'b0),
		.LOAD_DATA(32'b0),
		.RST(rst)
	);
	
	MACC_MACRO #(
		.DEVICE("SPARTAN6"),
		.LATENCY(1),
		.WIDTH_A(DATA_WIDTH),
		.WIDTH_B(DATA_WIDTH),
		.WIDTH_P(2*DATA_WIDTH)
	) macc7 (
		.P(c7),
		.A(amat[2][idx]),
		.ADDSUB(1'b1),
		.B(bmat[idx][1]),
		.CARRYIN(1'b0),
		.CE(en && !done),
		.CLK(clk),
		.LOAD(1'b0),
		.LOAD_DATA(32'b0),
		.RST(rst)
	);
	
	MACC_MACRO #(
		.DEVICE("SPARTAN6"),
		.LATENCY(1),
		.WIDTH_A(DATA_WIDTH),
		.WIDTH_B(DATA_WIDTH),
		.WIDTH_P(2*DATA_WIDTH)
	) macc8 (
		.P(c8),
		.A(amat[2][idx]),
		.ADDSUB(1'b1),
		.B(bmat[idx][2]),
		.CARRYIN(1'b0),
		.CE(en && !done),
		.CLK(clk),
		.LOAD(1'b0),
		.LOAD_DATA(32'b0),
		.RST(rst)
	);

	always @ (posedge clk)
	begin
	
		if (rst == 1'b1)
		begin
			done <= 1'b0;
			num_cycles_ran <= 0;
		end
		
		// calculation has not been completed
		else if (num_cycles_ran < NUM_CYCLES_REQ)
		begin
		
			// do MACC only on enable
			if (en == 1'b1)
			begin
			
				num_cycles_ran <= num_cycles_ran + 1'b1;

				if (idx == 2'b10)
				begin
					idx <= 2'b0;
				end
				
				else
				begin
					idx <= idx + 2'b01;
				end
		
			end // en
			
		end // num_cycles_ran
		
		// results are ready
		else
		begin
			done <= 1'b1;
		end
			
	end

endmodule
