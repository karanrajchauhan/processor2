`timescale 1ns / 1ps

// TODO: maybe just remove output en from memory?
// TODO: add read data input to mux for selecting write data to reg file.

module CPU (
	input clk,
	input rst,
	input en,
	output halt,
	output glb_is_equal
);


//////////////////////////////// CONTOL VARIABLES ////////////////////////////////

	// global
	reg 	[3:0]	pc;		
	wire 	[15:0]	instr;
	reg 	[15:0]	imem[0:15];		// use this to simulate instruction memory

	// reg io
	wire 	[5:0] 	write_sel;
	wire 	[15:0]	reg_write_data;
	wire 	[15:0]	read1_data;
	wire 	[15:0]	read2_data;

	// control unit io
	// wire 			halt;
	wire 			jump;
	wire 			mem_write_en;
	wire 			reg_write_en;
	wire 			reg_write_data_sel;

	// alu io
	wire 	[15:0]	alu_result;

	// data memory io
	wire 	[3:0]	mem_address;
	wire 	[15:0]	mem_read_data;
	wire 	[15:0]	mem_write_data;


///////////////////////////////////// MODULES ////////////////////////////////////

	ControlUnit control_unit (
		.clk               (clk),
		.rst               (rst),
		.glb_is_equal      (glb_is_equal),
		.instr_opcode      (instr[15:12]),
		.halt              (halt),
		.jump              (jump),
		.reg_write         (reg_write_en),
		.mem_write         (mem_write_en),
		.reg_write_data_sel(reg_write_data_sel)
	);

	RegisterFile reg_file (
		.clk         (clk),
		.rst         (rst),
		.write_en    (reg_write_en),
		.instr_opcode(instr[15:12]),
		.read1_sel   (instr[11:6]),
		.read2_sel   (instr[5:0]),
		.write_sel   (write_sel),
		.write_data  (reg_write_data),
		.read1_data  (read1_data),
		.read2_data  (read2_data)
	);

	ALU alu (
		.clk         (clk),
		.rst         (rst),
		.alu_op      (instr[15:12]),
		.operand_A   (read1_data),
		.operand_B   (read2_data),
		.result      (alu_result),
		.glb_is_equal(glb_is_equal)
	);

	CRMemory dmem (
		.clk       (clk),
		.rst       (rst),
		.output_en (1'b1),
		.write_en  (mem_write_en),
		.address   (mem_address),
		.write_data(mem_write_data),
		.read_data (mem_read_data)
	);


////////////////////////////////// CONTOL LOGIC //////////////////////////////////

	assign glb_is_equal = 0;

	// pc just reads from instruction memory
	assign instr = imem[pc];

	// // done if reached halt
	// assign done = instr==16'b0 ? 1'b1 : 1'b0;

	// use Rn as write reg always EXCEPT when inc instruction because it uses 12 bits to select register
	assign write_sel = instr[15:12]==4'b0001 ? instr[5:0] : instr[11:6];

	// if 0 then use alu result as write data else use memory data
	assign reg_write_data = reg_write_data_sel==0 ? read2_data 
								 : mem_read_data;

	// todo
	assign mem_address = alu_result;

	// DEBUG
//	reg mem0;
//	reg r0;
//	reg r1;
//	reg r2;
//	reg r3;
//	reg r4;
//	reg r5;

	always @ (posedge clk)
	begin

		// debug
//		mem0 <= dmem.memory[0];
//		r0 <= reg_file.registers[0];
//		r1 <= reg_file.registers[1];
//		r2 <= reg_file.registers[2];
//		r3 <= reg_file.registers[3];
//		r4 <= reg_file.registers[4];
//		r5 <= reg_file.registers[5];

		imem[0] <= 16'b1010_000011_000000;	// mov r3, r0
		imem[1] <= 16'b1001_000011_110010;	// mov r3, 110010
		imem[2] <= 16'b1100_000001_000010;	// mov r1, [r2] -- load data from mem[R2data] into r1
		imem[3] <= 16'b0001_000000_000100;	// inc r4
		imem[4] <= 16'b0101_000100_000101;	// add r4, r5
		imem[5] <= 16'b0110_000100_000101;	// sub r4, r5
		imem[6] <= 16'b0111_000010_000001;	// xor r2, r1
		imem[7] <= 16'b1101_000100_000011;	// sadd r4, r3
		imem[8] <= 16'b1011_000000_000001;	// mov [r0], r1 -- put data to mem[R0data] from r1
		imem[9] <= 16'b1000_000001_000010;	// cmp r1, r2
		imem[10] <= 16'b0011_000000_111111;	// jne 63
		imem[11] <= 16'b0100_000000_111111;	// je 63
		imem[12] <= 16'b0010_000000_111111;	// jmp 63
		imem[13] <= 16'b0000_000000_000000;	// halt
		// imem[14] <= 16'b0000_000000_000000;	// halt
		// imem[15] <= 16'b0000_000000_000000;	// halt

		if (rst == 1'b1)
		begin
			pc <= 0-1;	// initialize such that it will be zero in the first cycle
			// done <= 0;
		end

		else if (en == 1'b1)
		begin
			// // done if reached halt
			// done <= instr==16'b0 ? 1'b1 : 1'b0;
			pc <= pc + 1'b1;
		end

	end

endmodule
